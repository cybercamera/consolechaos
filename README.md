consolechaos
============

Name
----

consolechaos - simple, console-based word game.

Synopsis
--------

Console (i.e, terminal) based word-game for Linux. The console is continuously populated with a number of randomly-generated characters and the player has to find as many actual legible words as possible.

Installation
------------

You can fetch consolechaos in source code form directly from the git repo [https://gitlab.com/cybercamera/consolechaos], or you can download a number of pre-built installable packages and install those in the normal manner by your operating system's package management process from https://sourceforge.net/projects/consolechaos/files/

Description
-----------

You launch consolechaos either from the ternminal command-line ('consolechaos.gambas') or via the menu item under which the application is installed on your operating system. 

Once launched, you will be presented with a screen that looks like this:

![consolechaos main screen](consolechaos_main_screen.png "consolechaos main screen")


